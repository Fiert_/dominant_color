const express = require('express');

const app = express();
const port = 3030;


const routes = require('./routes/routes')(app);

app.listen(port, (err) => err ? console.log('Error in', err) : console.log(`Server is listening on port: ${port}`));
app.use(express.static('public'));
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
