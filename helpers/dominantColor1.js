const im = require('imagemagick');

module.exports = function (path, opts, next) {
    if (typeof opts === 'function') {
        next = opts;
        opts = undefined;
    }
    if (!next) {
        next = function () {
        };
    }
    if (!opts) {
        opts = {};
    }
    if (!opts.format) {
        opts.format = 'hex';
    }
   // const imArgs = [path, '-depth', '8', '-unique-colors', '-colorspace', 'HSL', 'txt:-'];
    const imArgs = [path, '+dither','-colors', '5','-define', 'histogram:unique-colors=true','-format', '%c', 'histogram:info:-'];
    // const imArgs = [path, '+dither',  '-colors', '5','-unique-colors', '-format', 'histogram:info:'];
    im.convert(imArgs, (err, stdout) => {
        if (err) {
            next(err);
        }
        console.log(stdout);
        console.log('11111111111111111111111111');
        let stdoutnew = stdout.substring(stdout.indexOf('2,0:'), stdout.indexOf('3,0:'));
       // console.log(stdoutnew.substring(stdoutnew.indexOf('('), stdoutnew.indexOf(')')));
        console.log('11111111111111111111111111');
        // const rgb = stdoutnew.substring(stdoutnew.indexOf('(') + 1, stdoutnew.indexOf(')'));
        const rgb = stdout.substring(stdout.indexOf('(') + 1, stdout.indexOf(')'));
        console.log(rgb);
        const results = {
            hex() {
                return require('rgb-hex').apply(this, rgb.split(','));
            },
            rgb() {
                return rgb.split(',');
            },
        };

        console.log('---------------------------');
        console.log(results.rgb());
        console.log('---------------------------');
        next(null, results[opts.format]());
    });
};

// white + red color
// 171: (124, 29,  8) #7C1D08 srgb(124,29,8)
// 185: (141, 22,  5) #8D1605 srgb(141,22,5)
// 7744: (253, 38,  2) #FD2602 srgb(253,38,2)
// 25547: (255,255,254) #FFFFFE srgb(255,255,254)
// 353: (255,231,221) #FFE7DD srgb(255,231,221)


//black + red color
// 27735: (  2,  0,  1) #020001 srgb(2,0,1)
// 365: ( 34,  0,  0) #220000 srgb(34,0,0)
// 356: (106, 19,  9) #6A1309 srgb(106,19,9)
// 7744: (249, 38,  2) #F92602 srgb(249,38,2)
// 200: (254,254,254) #FEFEFE srgb(254,254,254)
