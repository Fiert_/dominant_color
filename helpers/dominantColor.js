const im = require('imagemagick');

module.exports = function (path, opts, next) {
  if (typeof opts === 'function') {
    next = opts;
    opts = undefined;
  }
  if (!next) { next = function () {}; }
  if (!opts) { opts = {}; }
  if (!opts.format) { opts.format = 'hex'; }

  const imArgs = [path, '+dither', '-colors', '5',  'txt:-'];
  im.convert(imArgs, (err, stdout) => {
    if (err) { next(err); }
    const rgb = stdout.substring(stdout.indexOf('(') + 1, stdout.indexOf(')'));

    const results = {
      hex() { return require('rgb-hex').apply(this, rgb.split(',')); },
      rgb() { return rgb.split(','); },
    };

    next(null, results[opts.format]());
  });
};
