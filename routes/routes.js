const fs = require('fs');
const path = require('path');
const util = require('util');
const dir = 'public';
const ColorThief = require('color-thief');
const AverageColor = require('../helpers/average-color');
const avgColor = require('../helpers/dominantColor1');

const image = 'public/image7.png';
const gm = require('gm').subClass({ imageMagick: true });


module.exports = (app) => {


    app.get('/', (req, res) => res.send('Test express'));
    app.get('/colorThief', (req, res) => {
        const averageColor = new AverageColor();
        let images = [];
        let files = fs.readdirSync(dir);
        files.map((elem) => {
            images.push({
                path : path.join(__dirname, '../public/' + elem),
                name : elem,
                color : averageColor.getColor(path.join(__dirname, '../public/' + elem))
            })
        });

        res.render('avgColors.pug', {Items: images})
    });

    app.get('/dominantColor', (req, res) => {
        let files = fs.readdirSync(dir);
        let images = [];
        let paths = [];
        let fileNames = [];
        let rgbs;
        const avgColors = util.promisify(avgColor);
        Promise.all(files.map(file => avgColors(dir + '/' + file, {format : 'rgb'})
            .then(paths.push(dir + '/' + file))
            .then(fileNames.push(file))))
            .then(result => {
                rgbs = result;
                for (let i = 0; i < paths.length; i++) {
                    images.push({path: paths[i], name: fileNames[i], color: rgbs[i]});
                }
                res.render('dominantColor.pug', {Items: images})
            })
            .catch(err => console.log(err));
    });

   // let file = fs.readFileSync(path.join(__dirname, '../public/' + 'sobaka.jpeg'));
    // console.log(file);
    // gm(file)
    //     .colors(1)
    //     .toBuffer('RGB', function (err, buffer) {
    //          console.log(buffer.slice(0,3).toString());
    //     })

};
